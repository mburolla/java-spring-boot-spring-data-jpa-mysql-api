package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;
import com.example.demo.Service.BookstoreService;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;

@SpringBootTest
public class BookstoreServiceTest {

    @Autowired
    private BookstoreService bookStoreService;

    @Test
    void getBook() {
        var book = bookStoreService.getBook(1);
        Assert.notNull(book, "Book has not been returned from the bookstore Service");
    }
}
