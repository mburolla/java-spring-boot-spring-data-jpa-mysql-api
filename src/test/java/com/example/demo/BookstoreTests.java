package com.example.demo;

import org.junit.jupiter.api.Test;
import com.example.demo.Model.Book;
import com.example.demo.Model.Bookstore;
import com.example.demo.Repository.BookRepository;
import com.example.demo.Repository.BookstoreRepository;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;

@SpringBootTest
public class BookstoreTests {
    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private BookstoreRepository bookstoreRepository;

    @Test
    void addBookstoreWithBook() {
        var bookstore = new Bookstore("Books and More Books", "321 Oak Ave", "Rochester", "NY", "14580");
        var book = new Book("4444444444", "Book Title 4");
        bookRepository.save(book); // child
        bookstore.addBook(book);
        bookstoreRepository.save(bookstore); // Owner
    }
}
