package com.example.demo.Repository;

import com.example.demo.Model.Bookstore;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookstoreRepository extends JpaRepository<Bookstore, Integer> {

}
