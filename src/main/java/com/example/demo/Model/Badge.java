package com.example.demo.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Badge {

    //
    // Data members
    //

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    private Boolean isActive;

    private Date activeDate;

    private Date deactivatedDate;

    //
    // Constructors
    //

    public Badge() {
    }

    public Badge(Boolean isActive, Date activeDate, Date deactivatedDate) {
        this.isActive = isActive;
        this.activeDate = activeDate;
        this.deactivatedDate = deactivatedDate;
    }

    //
    // Accessors
    //

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Date getActiveDate() {
        return activeDate;
    }

    public void setActiveDate(Date activeDate) {
        this.activeDate = activeDate;
    }

    public Date getDeactivatedDate() {
        return deactivatedDate;
    }

    public void setDeactivatedDate(Date deactivatedDate) {
        this.deactivatedDate = deactivatedDate;
    }

    //
    // Overrides
    //

    @Override
    public String toString() {
        return "Badge{" +
                "Id=" + Id +
                ", isActive=" + isActive +
                ", activeDate=" + activeDate +
                ", deactivatedDate=" + deactivatedDate +
                '}';
    }
}
