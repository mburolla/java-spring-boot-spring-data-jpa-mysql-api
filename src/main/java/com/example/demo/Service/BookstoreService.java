package com.example.demo.Service;

import com.example.demo.Model.Book;
import com.example.demo.Repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookstoreService {

    @Autowired
    private BookRepository bookRepository;

    public Book getBook(Integer bookId) {
        var book = bookRepository.findById(bookId).get();
        return book;
    }

    public void addBook(Book book) {
        bookRepository.save(book);
    }
}
