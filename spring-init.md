# Creating Your First Spring Boot Web API
Note: These steps are NOT required for this repository.  This is here solely as a reference.
- Navigate to the [Spring Initilizr](https://start.spring.io/)
- Add the following dependencies:
    - Spring Web
    - MySQL Driver
    - Spring Data JPA

- Unzip file, Open in IntelliJ (select Pom file)
- Replace `xxx` with your values and add the following to: `src/main/resources/application.properties`
```
  spring.datasource.driver-class-name = com.mysql.cj.jdbc.Driver
  spring.datasource.url = jdbc:mysql://localhost:3306/xxx
  spring.datasource.username = xxx
  spring.datasource.password = xxx
  spring.jpa.hibernate.ddl-auto = update
  spring.jpa.show-sql = true
  spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.MySQL5Dialect
  spring.jpa.properties.format_sql = true
```

- Create package called Controller
- Create a health controller in the Controller package:
```
@RestController
public class HealthController {
    @GetMapping("api/v1/health")
    public ResponseEntity<String> getHealther() {
        return new ResponseEntity<String>("OK", HttpStatus.OK);
    }
}
```
- Test health controller: `http://localhost:8080/api/v1/health` should return "OK"
